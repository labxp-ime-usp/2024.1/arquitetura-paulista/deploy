# deploy

Instruções para criação de servidor e deploy da aplicação Arquitetura Paulista

## Preparação do Servidor

Servidor Ubuntu Server 22.04

### Instalação de Software
Necessário possuir permissão root
instalar apache, docker, docker-compose e mysql:

`apt install -y docker docker-compose mysql apache2`

Instala modulo proxy:
`a2enmod proxy_http`

### Configuração

1. Definir uma senha para o usuario capusp do banco de dados, utilizado pela aplicação.
1. Alterar os arquivos `.env` e `init_db.sql` desse repositório com o valor da senha nos campos especificados
1. Executar o comando para criar e inicializar o banco de dados: `mysql < init_db.sql`
1. Copiar o arquivo sites_enabled.conf para o diretório de configuração Apache: `cp sites_available.conf /etc/apache2/sites-available/000-default.conf`
1. Criar diretório `/var/www/cap_front_end` com o utilizando usuário root.

## Deploy da aplicação

### Back-end
Para disponibilizar a aplicação, é necessário executar o docker compose no diretório que contém o arquivo `compose.yaml` e o `.env`. Utilize o comando a seguir: `docker-compose up -d`

Para atualizar o back-end, siga os seguintes passos (pode ser necessário `sudo`):

1. Desative o container atual com `docker-compose rm -fsv`
2. Atualize a imagem atual com `docker-compose pull`
3. Suba o container novamente com `docker-compose up -d`


Para acompanhar os logs da aplicação: `docker logs arquitetura-paulista_backend_1`

Para apagar a imagem atual: `docker rmi registry.gitlab.com/labxp-ime-usp/2024.1/arquitetura-paulista/cap-back-end`


### Front-end
Para disponibilizar o front-end, é necessário baixar o novo pacote e disponibilizar no caminho `/var/www/cap_front_end` e reiniciar o apache. Esse processo foi simplificado com o script `deploy_front.sh`

Para deploy, executar o comando `./deploy_front.sh` com o usuário root (por conta da escrita no `/var/www` e do restart do serviço apache).

## Restart servidor Apache:
Realizar o restart do servidor apache com os comandos: 
1. Start: `systemctl start apache2`
1. Restart: `systemctl restart apache2`
1. Stop: `systemctl stop apache2`

## SSL

Após a configuração inicial, ativar SSL seguindo os passos do [certbot](https://certbot.eff.org/instructions?ws=apache&os=ubuntufocal) para apache e Ubuntu.

As requisições chegarão ao servidor utilizando HTTPS, e o Apache redirecionará ao Backend com HTTP.