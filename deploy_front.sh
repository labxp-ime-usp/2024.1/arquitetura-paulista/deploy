WEBSITE_DIR=/var/www/cap_front_end

echo "Removendo site atual..."
cd $WEBSITE_DIR
rm -rv *

echo "Download do novo pacote..."
curl --output front.tar.gz --location "https://gitlab.com/api/v4/projects/56401821/packages/generic/cap-frontend/0.0.1/cap-frontend.tar.gz"

echo "Descompactando arquivos..."
tar -vxf front.tar.gz && rm front.tar.gz
mv dist/cap-front-end/* . && rm -r dist

echo "Restart do Apache..."
systemctl restart apache2
systemctl status apache2

echo "- FIM -"